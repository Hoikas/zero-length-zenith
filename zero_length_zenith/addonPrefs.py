# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


import bpy, os
from bpy.props import *

# Shamelessly inspired by Korman !
class ZlzAddonPreferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    def checkSourceTexPath(self, context):
        self.moulSourcesTexPathValid = os.path.abspath(self.moulSourcesTexPath) \
                                         .replace('\\', '/') \
                                         .endswith("/sources/textures/tga") \
                                       and os.path.exists(self.moulSourcesTexPath)

    moulSourcesTexPath = StringProperty(name="MOUL source textures",
                                        description="Path to 'TGA' folder from the moul-assets repository (to load higher res textures)",
                                        options=set(),
                                        subtype="FILE_PATH",
                                        update=checkSourceTexPath,
                                        default="C:/Path/To/moul-assets/sources/textures/tga")

    moulSourcesTexPathValid = BoolProperty(options={"HIDDEN", "SKIP_SAVE"})

    def draw(self, context):
        layout = self.layout
        layout.alert = not self.moulSourcesTexPathValid
        layout.prop(self, "moulSourcesTexPath")
