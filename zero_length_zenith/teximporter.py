# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Operator used for importing ALL textures from the game.
Only useful if you want all of Uru's textures in a single place for some reason...
#"""

import bpy, os, time, sys, ctypes
from bpy.props import *
from zero_length_zenith.log import Log
import zero_length_zenith.utils
from zero_length_zenith.imageImporter import ImageImporter
from zero_length_zenith.sceneImporter import SceneImporter
import PyHSPlasma as pl


#-#-#-# Registration #-#-#-#

def menu_func_import(self, context):
    self.layout.operator(TexImporter.bl_idname, text="ZLZ batch texture extractor")


def register():
    bpy.types.INFO_MT_file_import.append(menu_func_import)

def unregister():
    bpy.types.INFO_MT_file_import.remove(menu_func_import)


#-#-#-# Import Class #-#-#-#

class TexImporter(bpy.types.Operator):
    """Extracts all textures from Uru's folder into TMP_Textures. Does not import meshes"""
    bl_idname = "import.zlzimport_tex"
    bl_label = "Texture extraction"

    filepath = StringProperty(subtype='FILE_PATH')

    reuseTextures = BoolProperty(name="Reuse textures", default=True,
                        description="This will skip extracting textures which are already in the TMP_Textures folder and load those instead")

    textureFormat = EnumProperty(name="Texture format",
                        items=(('PNG', "PNG", "Convert to PNG. More widely supported than DDS, but reimporting in 3D engines will degrade quality due to the successive lossy compressions."),
                               ('ORIGINAL', "Original (DDS)", "Extract as DDS (or the original file format when possible). Not all softwares support DDS, but this means you can reuse textures in other engines without worrying about degrading texture quality.")),
                        default="ORIGINAL",
                        description="Format to extract textures in")

    def __init__(self):
        self.imageImporter = None
        self.importLocation = ""
        self.texFolderLocation = None
        self.moulSourcesTexPathValid = False # We care about extracting textures only.

    def execute(self, context):
        """Executes import, called after selecting the file in Blender"""

        # first, force the console visible on Windoz (simplified version of code stolen from Korman)
        if sys.platform == "win32":
            hwnd = ctypes.windll.kernel32.GetConsoleWindow()
            if not bool(ctypes.windll.user32.IsWindowVisible(hwnd)):
                bpy.ops.wm.console_toggle()
                ctypes.windll.user32.ShowWindow(hwnd, 1)
                ctypes.windll.user32.BringWindowToTop(hwnd)

        start = time.clock()
        filePath = self.filepath
        self.config = config = {
            'textureFormat': self.textureFormat,
            'reuseTextures': self.reuseTextures,
        }

        if filePath[-1] != '/' and filePath[-1] != '\\': # remove any filename
            filePath = filePath[:filePath.rfind('/')]
            filePath = filePath[:filePath.rfind('\\')]

        log=Log(sys.stdout, filePath+"all.log", "w")
        sys.stdout=log
        self.imageImporter = ImageImporter(self)
        for age in os.listdir(filePath):
            print("age: " + age)
            if not os.path.isfile(filePath + os.sep + age):
                continue
            if not age.lower().endswith(".age"):
                continue
            self.importTexes(filePath + os.sep + age, config)
        sys.stdout = sys.__stdout__
        log.close()
        print("Done in %.2f seconds." % (time.clock()-start))
        return {"FINISHED"}

    def importTexes(self, filePath, config):
        print("\n\n--------------------------------------\n---> Importing %s" % filePath)

        rmgr = pl.plResManager()
        age = rmgr.ReadAge(filePath, True)
        print("Age name is %s" % age.name)

        # Force Korman active - we need it to build cubemaps.
        try:
            bpy.context.scene.render.engine = "PLASMA_GAME"
        except TypeError:
            raise TypeError("Korman needs to be installed and active before importing can take place.")

        self.importLocation = os.path.dirname(filePath)
        texPath = self.importLocation + os.sep + "TMP_Textures"
        if not os.path.exists(texPath):
            os.mkdir(texPath)
        self.texFolderLocation = texPath

        knownTexes = []
        for location in rmgr.getLocations():
            if location.prefix != age.seqPrefix:
                # we didn't load any other Age, how can this even happen ? whatever, just ignore it
                continue
            texKeys = rmgr.getKeys(location, pl.plFactory.kMipmap)
            for tex in texKeys:
                if tex not in knownTexes:
                    self.imageImporter.getImage(tex)
                    knownTexes.append(tex)
            texKeys = rmgr.getKeys(location, pl.plFactory.kCubicEnvironmap)
            for tex in texKeys:
                if tex not in knownTexes:
                    self.imageImporter.getCubemap(tex)
                    knownTexes.append(tex)
        rmgr.UnloadAge(age.name)

        print("\n\nDone !")
        return {"FINISHED"}

    def validKey(self, plKey):
        """Returns true if the given plKey is valid, and if the object it references is loaded. Returns false otherwise.
        The return value is the same as importer.validKey(), except there is no warning for missing references (not interesting for the texture extractor)."""
        if plKey and plKey.object:
                return True
        return False

    def invoke(self, context, event):
        """Displays filepicker interface, after selecting import menu"""
        print("\nZLZ Uru Importer: Be Invoked !")
        if not self.filepath:
            self.filePath = bpy.path.ensure_ext(bpy.data.filepath, ".age")
        WindowManager = context.window_manager
        WindowManager.fileselect_add(self)
        return {'RUNNING_MODAL'}
