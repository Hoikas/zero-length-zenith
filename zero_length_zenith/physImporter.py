# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing physics: regions and colliders.
Imported colliders will be displayed as wireframe in the viewport, and hidden from rendering.

Known bugs:
- Gahreesen's well collision is somehow too high. Its local transform should be reset to zero.
  This is likely some subworld shenanigans. No other object has the issue, so let's not bother with it.
- Some of MOULa's colliders are STILL not positioned correctly ? Should be OK for 99% of the time though.
#"""

import bpy, bmesh
from mathutils import *
import PyHSPlasma as pl
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *
import zero_length_zenith.sphere

class PhysImporter:

    def __init__(self, parent):
        self.parent = parent
        self.collisionsInWorldSpace = [] # those will be unparented and reset to world origin later

    def importPhysical(self, sceneObjKey, simKey):
        version = sceneObjKey.location.version

        if not self.parent.validKey(simKey) or not self.parent.validKey(simKey.object.physical):
            return None

        sim = simKey.object
        physKey = sim.physical
        phys = physKey.object

        mesh = bpy.data.meshes.new(simKey.name)
        blObj = bpy.data.objects.new(sceneObjKey.name + "_COLLISION", mesh)
        isCollider = phys.memberGroup not in (pl.plSimDefs.kGroupDetector, pl.plSimDefs.kGroupLOSOnly)
        self.parent.sceneImporter.appendObjectToScenes(blObj, SceneImporter.layerColliders if isCollider else SceneImporter.layerRegions)
        blObj.draw_type = "WIRE"
        blObj.hide_render = True

        if isCollider:
            # setup Korman collision modifier
            blObj.select = True
            bpy.context.scene.objects.active = blObj
            bpy.ops.object.plasma_modifier_add(types="collision")
            kormanCollision = blObj.plasma_modifiers.collision
            blObj.select = False

            if phys.boundsType == pl.plSimDefs.kHullBounds:
                kormanCollision.bounds = "hull"
            elif phys.boundsType == pl.plSimDefs.kProxyBounds or phys.boundsType == pl.plSimDefs.kExplicitBounds:
                kormanCollision.bounds = "trimesh"
            elif phys.boundsType == pl.plSimDefs.kSphereBounds or phys.boundsType == pl.plSimDefs.kCylinderBounds:
                kormanCollision.bounds = "sphere"
            elif phys.boundsType == pl.plSimDefs.kBoxBounds:
                kormanCollision.bounds = "box"

            kormanCollision.avatar_blocker = phys.memberGroup == pl.plSimDefs.kGroupStatic or phys.memberGroup == pl.plSimDefs.kGroupDynamic
            kormanCollision.camera_blocker = hasFlags(phys.LOSDBs, pl.plSimDefs.kLOSDBCameraBlockers)
            kormanCollision.friction = phys.friction
            kormanCollision.restitution = phys.restitution
            kormanCollision.terrain = hasFlags(phys.LOSDBs, pl.plSimDefs.kLOSDBAvatarWalkable)
            kormanCollision.dynamic = phys.memberGroup == pl.plSimDefs.kGroupDynamic
            kormanCollision.mass = phys.mass
            kormanCollision.start_asleep = phys.getProperty(pl.plSimulationInterface.kStartInactive)

        # pos/rot properties:
        # available to offset/rotate the collision vertices.
        # In PotS, it seems Plasma uses this as a hack to place physical objects which have mass != 0
        # (meaning a coordinate interface), but leaves those unused for objects with mass == 0
        # (objects without coordint, which means vertices are expressed in world-space).
        # In MOUL, I'm not sure how this mess works, but it seems we can ignore it >.>
        quatRot = Quaternion((phys.rot.W, phys.rot.X, phys.rot.Y, phys.rot.Z))
        vectPos = Vector((phys.pos.X, phys.pos.Y, phys.pos.Z))

        if quatRot.magnitude == 0:
            # looks like it's another quat with dumped W. Ignore it.
            quatRot = Quaternion((1, 0, 0, 0))

        if version == pl.pvPots:
            if phys.mass != 0 or phys.getProperty(pl.plSimulationInterface.kPhysAnim):
                quatRot.identity()
                vectPos = Vector()
            else:
                self.collisionsInWorldSpace.append(blObj)
        elif version == pl.pvMoul:
            # This is silly. And random.
            # Although it works as far as I can tell...
            # (yeah, even works for Tsogal's horrible footstep sound region and their partially negative non-uniform scaling...)
            if phys.mass == 0 \
                    and not phys.getProperty(pl.plSimulationInterface.kPhysAnim) \
                    and not phys.getProperty(pl.plSimulationInterface.kPinned) \
                    and phys.memberGroup != pl.plSimDefs.kGroupDetector \
                    and phys.boundsType in (pl.plSimDefs.kHullBounds, pl.plSimDefs.kBoxBounds) \
                    and not vectPos.magnitude: # we done with stupid rules yet ?
                self.collisionsInWorldSpace.append(blObj)
            # ↓ it appears it's best to ignore those when dealing with MOULa PRPs. Might be just hints for the physics system or something.
            quatRot.identity()
            vectPos = Vector()

        def addVert(bm, vert):
            vert = Vector(vert)
            vert.rotate(quatRot)
            vert += vectPos
            bm.verts.new((vert.x, vert.y, vert.z))

        if phys.boundsType in (
                pl.plSimDefs.kHullBounds,
                pl.plSimDefs.kProxyBounds,
                pl.plSimDefs.kExplicitBounds) \
                or len(phys.verts):
            bm = bmesh.new()

            # this is thankfully much easier than importing actual drawable meshes...
            for vert in phys.verts:
                addVert(bm, (vert.X, vert.Y, vert.Z))

            bm.verts.ensure_lookup_table()
            for i in range(len(phys.indices) // 3):
                try:
                    bm.faces.new((
                        bm.verts[phys.indices[i * 3]],
                        bm.verts[phys.indices[i * 3 + 1]],
                        bm.verts[phys.indices[i * 3 + 2]],
                    ))
                except ValueError:
                    pass

            if not len(phys.indices) and len(phys.verts) >= 3:
                # this is a convex hull - try to generate faces
                bmesh.ops.convex_hull(bm, input=bm.verts, use_existing_faces=False)

            # just in case
            bmesh.ops.remove_doubles(bm, verts=bm.verts, dist=.001)

            bm.to_mesh(mesh)
            bm.free()
            del bm

        elif phys.boundsType == pl.plSimDefs.kBoxBounds:
            # Uru CC boxes are instead convex colliders made from a list of verts, which are processed above.
            # This is only if you try to import a MOUL/MV Age directly...
            bm = bmesh.new()
            offset = phys.offset
            smallestX = -phys.dimensions.X + offset.X
            smallestY = -phys.dimensions.Y + offset.Y
            smallestZ = -phys.dimensions.Z + offset.Z
            biggestX = phys.dimensions.X + offset.X
            biggestY = phys.dimensions.Y + offset.Y
            biggestZ = phys.dimensions.Z + offset.Z
            addVert(bm, (smallestX, smallestY, smallestZ))
            addVert(bm, (biggestX,  smallestY, smallestZ))
            addVert(bm, (biggestX,  biggestY,  smallestZ))
            addVert(bm, (smallestX, biggestY, smallestZ))
            addVert(bm, (smallestX, biggestY, biggestZ))
            addVert(bm, (smallestX, smallestY, biggestZ))
            addVert(bm, (biggestX, smallestY, biggestZ))
            addVert(bm, (biggestX, biggestY, biggestZ))
            bm.verts.ensure_lookup_table()

            bmesh.ops.convex_hull(bm, input=bm.verts, use_existing_faces=False)

            bm.to_mesh(mesh)
            bm.free()
            del bm

        elif phys.boundsType == pl.plSimDefs.kSphereBounds:
            # no way around it... programmatically create a sphere
            bm = bmesh.new()

            for vert in zero_length_zenith.sphere.vertices:
                addVert(bm, [vert[0] * phys.radius, vert[1] * phys.radius, vert[2] * phys.radius])
            bm.verts.ensure_lookup_table()
            for face in zero_length_zenith.sphere.faces:
                bm.faces.new((bm.verts[index] for index in face))
            bm.to_mesh(mesh)
            bm.free()
            del bm

        elif phys.boundsType == pl.plSimDefs.kCylinderBounds:
            # theoretically, this is only ever used by Laki's pirahna birds. No need to implement it.
            print("WARNING: Cylinder bounds not yet supported.")

            # okay, let's just build a sphere here too.
            bm = bmesh.new()

            for vert in zero_length_zenith.sphere.vertices:
                addVert(bm, [vert[0] * phys.radius, vert[1] * phys.radius, vert[2] * phys.radius])
            bm.verts.ensure_lookup_table()
            for face in zero_length_zenith.sphere.faces:
                bm.faces.new((bm.verts[index] for index in face))
            bm.to_mesh(mesh)
            bm.free()
            del bm

        else:
            raise RuntimeError("Unknown collision ? type=%d" % phys.boundsType)

        return blObj

    def repositionWorldSpacePhysicals(self):
        # objects expressed in world-space will be at the wrong location.
        # Transform their geometry to match the expected location
        for obj in self.collisionsInWorldSpace:
            matrix = obj.matrix_world
            obj.data.transform(matrix.inverted())

