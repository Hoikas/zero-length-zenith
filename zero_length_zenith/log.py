# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Used for logging to both the console and a log file.
#"""

class Log:
    def __init__(self, handle, filename, mode="w"):
        self.file=open(filename, mode)
        self.handle=handle

    def write(self, x):
        self.handle.write(x)
        self.file.write(x)

    def flush():
        self.handle.flush()
        self.file.flush()

    def close(self):
        self.file.close()