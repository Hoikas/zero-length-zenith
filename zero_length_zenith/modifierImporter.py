# -*- coding:utf-8 -*-

"""
This file is part of ZLZ.

ZLZ is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

ZLZ is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with ZLZ.  If not, see <https://www.gnu.org/licenses/>
"""


"""
Handles importing and setting up the Korman equivalent to some of Plasma's modifiers/interfaces.
#"""

import bpy, bmesh
import PyHSPlasma as pl
from mathutils import *
from math import *
from zero_length_zenith.sceneImporter import SceneImporter
from zero_length_zenith.utils import *

class ModifierImporter:

    def __init__(self, parent):
        self.parent = parent
        self.setupReferenceLambdas = []

    def importModifiers(self, sceneObjKey, blObj):
        sceneObj = sceneObjKey.object

        blObj.select = True
        bpy.context.scene.objects.active = blObj

        objScenes = []
        for scene in bpy.data.scenes:
            if blObj.name in scene.objects:
                objScenes.append(scene)

        for interface in sceneObj.interfaces:

            if not self.parent.validKey(interface):
                # Modifier missing or failed to load. Skip.
                continue

            if interface.type == pl.plFactory.kHKSubWorld:
                bpy.ops.object.plasma_modifier_add(types="subworld_def")
                plMod = interface.object
                kormanMod = blObj.plasma_modifiers.subworld_def
                kormanMod.gravity = Vector((plMod.gravity.X, plMod.gravity.Y, plMod.gravity.Z))
                blObj.empty_draw_type = "SPHERE"

            elif interface.type == pl.plFactory.kSwimRegionInterface:
                plMod = interface.object
                bpy.ops.object.plasma_modifier_add(types="swimregion")
                kormanMod = blObj.plasma_modifiers.swimregion
                kormanMod.current_type = "NONE"
                kormanMod.down_buoyancy = plMod.downBuoyancy
                kormanMod.up_buoyancy = plMod.upBuoyancy
                kormanMod.up_velocity = plMod.maxUpwardVel

            elif interface.type == pl.plFactory.kSwimCircularCurrentRegion:
                plMod = interface.object
                bpy.ops.object.plasma_modifier_add(types="swimregion")
                kormanMod = blObj.plasma_modifiers.swimregion
                kormanMod.current_type = "CIRCULAR"
                kormanMod.down_buoyancy = plMod.downBuoyancy
                kormanMod.up_buoyancy = plMod.upBuoyancy
                kormanMod.up_velocity = plMod.maxUpwardVel

                # circular-specific properties
                kormanMod.rotation = plMod.rotation
                kormanMod.near_distance = sqrt(plMod.pullNearDistSq)
                kormanMod.far_distance = sqrt(plMod.pullFarDistSq)
                kormanMod.near_velocity = plMod.pullNearVel
                kormanMod.far_velocity = plMod.pullFarVel
                def setCurrent(kormanMod, plMod):
                    kormanMod.current = self.parent.getBlObjectFromKey(plMod.currentObj)
                self.setupReferenceLambdas.append(lambda: setCurrent(kormanMod, plMod))

            elif interface.type == pl.plFactory.kSwimStraightCurrentRegion:
                plMod = interface.object
                bpy.ops.object.plasma_modifier_add(types="swimregion")
                kormanMod = blObj.plasma_modifiers.swimregion
                kormanMod.current_type = "STRAIGHT"
                kormanMod.down_buoyancy = plMod.downBuoyancy
                kormanMod.up_buoyancy = plMod.upBuoyancy
                kormanMod.up_velocity = plMod.maxUpwardVel

                # straight-specific properties
                kormanMod.near_distance = plMod.nearDist
                kormanMod.far_distance = plMod.farDist
                kormanMod.near_velocity = plMod.nearVel
                kormanMod.far_velocity = plMod.farVel
                def setCurrent(kormanMod, plMod):
                    kormanMod.current = self.parent.getBlObjectFromKey(plMod.currentObj)
                self.setupReferenceLambdas.append(lambda: setCurrent(kormanMod, plMod))

            elif interface.type == pl.plFactory.kVisRegion:
                # ignore it, objectImporter and softVolumeImporter handle this
                pass

            elif interface.type == pl.plFactory.kSoftVolumeSimple:
                # ignore it, softVolumeImporter handles this
                pass

            elif interface.type in (pl.plFactory.kPrintShape, pl.plFactory.kActivePrintShape):
                plMod = interface.object
                bpy.ops.object.plasma_modifier_add(types="decal_print")
                kormanMod = blObj.plasma_modifiers.decal_print
                kormanMod.width = plMod.width
                kormanMod.length = plMod.length
                kormanMod.height = plMod.height
                if interface.type == pl.plFactory.kActivePrintShape:
                    for plMgrKey in plMod.decalMgrs:
                        mgr = kormanMod.managers.add()
                        mgr.name = plMgrKey.name # don't even need to check whether it was imported yet, yay.

        for modif in sceneObj.modifiers:

            if not self.parent.validKey(modif):
                # Modifier missing or failed to load. Skip.
                continue

            if modif.type == pl.plFactory.kSpawnModifier:
                bpy.ops.object.plasma_modifier_add(types="spawnpoint")
                blObj.empty_draw_type = "CONE" # faces a direction opposite to the avatar and it's not customizable grmbrml

            elif modif.type == pl.plFactory.kAvLadderMod:
                bpy.ops.object.plasma_modifier_add(types="laddermod")
                plLadderMod = modif.object
                kormanLadderMod = blObj.plasma_modifiers.laddermod
                kormanLadderMod.is_enabled = plLadderMod.enabled
                kormanLadderMod.direction = "UP" if plLadderMod.goingUp else "DOWN"
                kormanLadderMod.num_loops = plLadderMod.loops
                blObj.empty_draw_type = "SINGLE_ARROW"

            elif modif.type == pl.plFactory.kSittingModifier:
                bpy.ops.object.plasma_modifier_add(types="sittingmod")
                plSittingMod = modif.object
                kormanSittingMod = blObj.plasma_modifiers.sittingmod

                approaches = []
                if hasFlags(plSittingMod.miscFlags, pl.plSittingModifier.kApproachFront):
                    approaches.append("kApproachFront")
                elif hasFlags(plSittingMod.miscFlags, pl.plSittingModifier.kApproachLeft):
                    approaches.append("kApproachLeft")
                elif hasFlags(plSittingMod.miscFlags, pl.plSittingModifier.kApproachRight):
                    approaches.append("kApproachRight")
                elif hasFlags(plSittingMod.miscFlags, pl.plSittingModifier.kApproachRear):
                    approaches.append("kApproachRear")
                kormanSittingMod.approach = set(approaches)
                # TODO - if you can figure those out given how messy LogicModifier are, congrats on you :)
                # kormanSittingMod.clickable_object =
                # kormanSittingMod.region_object =
                # kormanSittingMod.facing_enabled =
                # kormanSittingMod.facing_degrees =

            elif modif.type == pl.plFactory.kImageLibMod:
                bpy.ops.object.plasma_modifier_add(types="imagelibmod")
                plLibMod = modif.object
                kormanLibMod = blObj.plasma_modifiers.imagelibmod
                for imageKey in plLibMod.images:
                    kormanLibMod.images.add().image = self.parent.imageImporter.getImage(imageKey)

            elif modif.type == pl.plFactory.kMaintainersMarkerModifier:
                bpy.ops.object.plasma_modifier_add(types="maintainersmarker")
                plMarker = modif.object
                kormanMarker = blObj.plasma_modifiers.maintainersmarker
                if plMarker.calibration == pl.plMaintainersMarkerModifier.kBroken:
                    kormanMarker.calibration = "kBroken"
                elif plMarker.calibration == pl.plMaintainersMarkerModifier.kRepaired:
                    kormanMarker.calibration = "kRepaired"
                elif plMarker.calibration == pl.plMaintainersMarkerModifier.kCalibrated:
                    kormanMarker.calibration = "kCalibrated"
                blObj.empty_draw_type = "ARROWS"

            elif modif.type == pl.plFactory.kShadowCaster:
                bpy.ops.object.plasma_modifier_add(types="rtshadow")
                plShadow = modif.object
                kormanShadow = blObj.plasma_modifiers.rtshadow
                kormanShadow.falloff = plShadow.attenScale * 100
                kormanShadow.blur = plShadow.blurScale * 100
                kormanShadow.boost = plShadow.boost * 100
                kormanShadow.limit_resolution = hasFlags(plShadow.castFlags, pl.plShadowCaster.kLimitRes)
                kormanShadow.self_shadow = hasFlags(plShadow.castFlags, pl.plShadowCaster.kSelfShadow)

            elif modif.type == pl.plFactory.kPanicLinkRegion:
                bpy.ops.object.plasma_modifier_add(types="paniclink")
                plRegion = modif.object
                kormanRegion = blObj.plasma_modifiers.paniclink
                kormanRegion.play_anim = plRegion.playLinkOutAnim

            elif modif.type == pl.plFactory.kViewFaceModifier:
                bpy.ops.object.plasma_modifier_add(types="viewfacemod")
                plModif = modif.object
                kormanModif = blObj.plasma_modifiers.viewfacemod

                kormanModif.preset_options = "Custom" # Ideally we should detect presets, but for simplicity we'll always use Custom to avoid errors.
                kormanModif.pivot_on_y = plModif.getFlag(pl.plViewFaceModifier.kPivotY)
                if plModif.getFlag(pl.plViewFaceModifier.kFaceCam):
                    kormanModif.follow_mode = "kFaceCam"
                elif plModif.getFlag(pl.plViewFaceModifier.kFaceList):
                    kormanModif.follow_mode = "kFaceList"
                elif plModif.getFlag(pl.plViewFaceModifier.kFacePlay):
                    kormanModif.follow_mode = "kFacePlay"
                elif plModif.getFlag(pl.plViewFaceModifier.kFaceObj):
                    kormanModif.follow_mode = "kFaceObj"

                    def setTarget(plModif, kormanModif, blObj):
                        kormanModif.target = self.parent.getBlObjectFromKey(plModif.faceObj)
                        # conveniency, yo
                        if kormanModif.pivot_on_y:
                            constraint = blObj.constraints.new("LOCKED_TRACK")
                            constraint.track_axis = "TRACK_NEGATIVE_Z"
                            constraint.lock_axis = "LOCK_Y"
                            constraint.target = kormanModif.target
                        else:
                            constraint = blObj.constraints.new("TRACK_TO")
                            constraint.track_axis = "TRACK_NEGATIVE_Z"
                            constraint.up_axis = "UP_Y"
                            constraint.target = kormanModif.target

                    self.setupReferenceLambdas.append(lambda: setTarget(plModif, kormanModif, blObj))
                if plModif.offset.X != 0 or plModif.offset.Y != 0 or plModif.offset.Z != 0:
                    kormanModif.offset = True
                    kormanModif.offset_local = plModif.getFlag(pl.plViewFaceModifier.kOffsetLocal)
                    kormanModif.offset_coord = Vector((plModif.offset.X, plModif.offset.Y, plModif.offset.Z))
                blObj.empty_draw_type = "CIRCLE"

            elif modif.type == pl.plFactory.kFollowMod:
                bpy.ops.object.plasma_modifier_add(types="followmod")
                plModif = modif.object
                kormanModif = blObj.plasma_modifiers.followmod

                options = []
                if hasFlags(plModif.mode, pl.plFollowMod.kPositionX):
                    options.append("kPositionX")
                if hasFlags(plModif.mode, pl.plFollowMod.kPositionY):
                    options.append("kPositionY")
                if hasFlags(plModif.mode, pl.plFollowMod.kPositionZ):
                    options.append("kPositionZ")
                if hasFlags(plModif.mode, pl.plFollowMod.kRotate):
                    options.append("kRotate")
                kormanModif.follow_mode = set(options)
                kormanModif.leader_type = "kFollowCamera"
                if plModif.leaderType == pl.plFollowMod.kFollowListener:
                    kormanModif.leader_type = "kFollowListener"
                if plModif.leaderType == pl.plFollowMod.kFollowPlayer:
                    kormanModif.leader_type = "kFollowPlayer"
                if plModif.leaderType == pl.plFollowMod.kFollowObject:
                    kormanModif.leader_type = "kFollowObject"
                if kormanModif.leader_type == "kFollowObject":
                    def setLeader(kormanModif, plModif):
                        kormanModif.leader = self.parent.getBlObjectFromKey(plModif.leader)
                    self.setupReferenceLambdas.append(lambda: setLeader(kormanModif, plModif))

                blObj.empty_draw_type = "CIRCLE"

            elif modif.type == pl.plFactory.kWaveSet7:
                bpy.ops.object.plasma_modifier_add(types="water_basic")
                bpy.ops.object.plasma_modifier_add(types="water_geostate")
                bpy.ops.object.plasma_modifier_add(types="water_texstate")
                plWaveSet = modif.object

                kormanBasic = blObj.plasma_modifiers.water_basic
                kormanGeo = blObj.plasma_modifiers.water_geostate
                kormanTex = blObj.plasma_modifiers.water_texstate

                # basic setup

                # wind
                if plWaveSet.getFlag(pl.plWaveSet7.kHasRefObject):
                    def setWind(kormanBasic, plWaveSet):
                        kormanBasic.wind_object = self.parent.getBlObjectFromKey(plWaveSet.refObj)
                    self.setupReferenceLambdas.append(lambda: setWind(kormanBasic, plWaveSet))
                else:
                    windObj = bpy.data.objects.new(modif.name + "_WIND", None)
                    lyrs = [False,]*20
                    lyrs[3] = True
                    for sce in objScenes:
                        sce.objects.link(windObj).layers = lyrs
                    windObj.empty_draw_type = "CONE"
                    windObj.empty_draw_size = plWaveSet.state.windDir.magnitude()
                    setLocalXForm(windObj, blObj.matrix_world.translation, Vector((
                        plWaveSet.state.windDir.X,
                        plWaveSet.state.windDir.Y,
                        plWaveSet.state.windDir.Z)).to_track_quat('Y','Z'))
                    kormanBasic.wind_object = windObj
                kormanBasic.wind_speed = plWaveSet.state.windDir.magnitude()

                # envmap
                if plWaveSet.envMap and plWaveSet.envMap.object:
                    # let's absue our privledges.
                    if plWaveSet.envMap.type == pl.plFactory.kCubicEnvironmap:
                        envmap = self.parent.imageImporter.getCubemap(plWaveSet.envMap)
                        kormanBasic.envmap = envmap
                        kormanBasic.envmap_radius = 10000
                    elif plWaveSet.envMap.type in (pl.plFactory.kDynamicEnvMap, pl.plFactory.kDynamicCamMap):
                        envmap, viewpoint = self.parent.imageImporter.getDynamicEnvMap(plWaveSet.envMap)
                        lyrs = [False,]*20
                        lyrs[3] = True
                        for sce in objScenes:
                            sce.objects.link(viewpoint).layers = lyrs
                        kormanBasic.envmap = envmap
                        kormanBasic.envmap_radius = envmap.environment_map.clip_end
                    else:
                        print("WARNING: envmap %s - unknown type %d" % (plWaveSet.envMap.name, plWaveSet.envMap.type))

                kormanBasic.specular_tint = Color((plWaveSet.state.specularTint.red, plWaveSet.state.specularTint.green, plWaveSet.state.specularTint.blue))
                kormanBasic.specular_alpha = plWaveSet.state.specularTint.alpha
                kormanBasic.noise = plWaveSet.state.specVector.X * 100
                kormanBasic.specular_start = plWaveSet.state.specVector.Y
                kormanBasic.specular_end = plWaveSet.state.specVector.Z
                kormanBasic.ripple_scale = plWaveSet.state.rippleScale
                kormanBasic.depth_opacity = plWaveSet.state.depthFalloff.X
                kormanBasic.depth_reflection = plWaveSet.state.depthFalloff.Y
                kormanBasic.depth_wave = plWaveSet.state.depthFalloff.Z
                kormanBasic.zero_opacity = -plWaveSet.state.waterOffset.X
                kormanBasic.zero_reflection = -plWaveSet.state.waterOffset.Y
                kormanBasic.zero_wave = -plWaveSet.state.waterOffset.Z

                # geo setup
                kormanGeo.min_length = plWaveSet.state.geoState.minLength
                kormanGeo.max_length = plWaveSet.state.geoState.maxLength
                kormanGeo.amplitude = plWaveSet.state.geoState.ampOverLen * 100
                kormanGeo.chop = plWaveSet.state.geoState.chop * 100
                kormanGeo.angle_dev = plWaveSet.state.geoState.angleDev

                # tex setup
                kormanTex.min_length = plWaveSet.state.texState.minLength
                kormanTex.max_length = plWaveSet.state.texState.maxLength
                kormanTex.amplitude = plWaveSet.state.texState.ampOverLen * 100
                kormanTex.chop = plWaveSet.state.texState.chop * 100
                kormanTex.angle_dev = plWaveSet.state.texState.angleDev

                # shores setup
                if len(plWaveSet.shores):
                    bpy.ops.object.plasma_modifier_add(types="water_shore")
                    kormanShore = blObj.plasma_modifiers.water_shore

                    for shoreKey in plWaveSet.shores:
                        shore = kormanShore.shores.add()
                        shore.display_name = shoreKey.name
                        def setShore(shore, shoreKey):
                            shore.shore_object = self.parent.getBlObjectFromKey(shoreKey)
                        self.setupReferenceLambdas.append(lambda: setShore(shore, shoreKey))
                    kormanShore.shore_tint = Vector((plWaveSet.state.minColor.red, plWaveSet.state.minColor.green, plWaveSet.state.minColor.blue))
                    kormanShore.shore_opacity = plWaveSet.state.minColor.alpha * 100
                    kormanShore.wispiness = plWaveSet.state.wispiness * 100
                    kormanShore.period = plWaveSet.state.period * 100
                    kormanShore.finger = plWaveSet.state.fingerLength * 100
                    kormanShore.edge_opacity = plWaveSet.state.edgeOpacity * 100
                    kormanShore.edge_radius = plWaveSet.state.edgeRadius * 100

            elif modif.type == pl.plFactory.kDistOpacityMod:
                bpy.ops.object.plasma_modifier_add(types="fademod")
                plMod = modif.object
                kormanMod = blObj.plasma_modifiers.fademod

                kormanMod.fader_type = "SimpleDist" if plMod.nearTrans == plMod.nearOpaq else "DistOpacity"
                kormanMod.near_trans = plMod.nearTrans
                kormanMod.near_opaq = plMod.nearOpaq
                kormanMod.far_trans = plMod.farTrans
                kormanMod.far_opaq = plMod.farOpaq

            elif modif.type == pl.plFactory.kFadeOpacityMod:
                bpy.ops.object.plasma_modifier_add(types="fademod")
                plMod = modif.object
                kormanMod = blObj.plasma_modifiers.fademod

                kormanMod.fader_type = "FadeOpacity"
                kormanMod.fade_in_time = plMod.fadeUp
                kormanMod.fade_out_time = plMod.fadeDown
                kormanMod.bounds_center = plMod.boundsCenter

            elif modif.type == pl.plFactory.kSubworldRegionDetector:
                bpy.ops.object.plasma_modifier_add(types="subworld_rgn")
                plMod = modif.object
                kormanMod = blObj.plasma_modifiers.subworld_rgn

                if plMod.subworld: # can be none: subworld exit regions without specified subworld will simply return to normal world.
                    def setupSubworld(plMod, blObj, kormanMod):
                        subworldObject = self.parent.getBlObjectFromKey(plMod.subworld)
                        if not subworldObject.plasma_modifiers.subworld_def.enabled:
                            blObj.select = False
                            subworldObject.select = True
                            bpy.context.scene.objects.active = subworldObject
                            bpy.ops.object.plasma_modifier_add(types="subworld_def")
                            subworldObject.empty_draw_type = "SPHERE"
                            subworldObject.select = False
                            blObj.select = True
                            bpy.context.scene.objects.active = blObj
                        kormanMod.subworld = subworldObject
                    self.setupReferenceLambdas.append(lambda: setupSubworld(plMod, blObj, kormanMod))
                kormanMod.transition = "exit" if plMod.onExit else "enter"

            elif modif.type == pl.plFactory.kRidingAnimatedPhysicalDetector: # actually a subworld region in Korman.
                bpy.ops.object.plasma_modifier_add(types="subworld_rgn")
                plMod = modif.object
                kormanMod = blObj.plasma_modifiers.subworld_rgn

                kormanMod.transition = "enter" if plMod.enterMsg else "exit" # actually, we can do both, but Korman can't.
                if plMod.enterMsg and plMod.exitMsg:
                    print("WARNING - %s: subworld detect region should be both enter and exit, but this is not supported.\n" % blObj.name
                        + "Please duplicate this region and set the duplicate to \"exit\"")

            elif modif.type == pl.plFactory.kResponderModifier:
                # responder modifiers contain LOTS of stuff.
                # For now, we're only interested in footstep sounds...

                plMod = modif.object
                for state in plMod.states:
                    for command in state.commands:
                        if command.msg and isinstance(command.msg, pl.plArmatureEffectStateMsg):
                            if self.parent.validKey(sceneObj.sim):
                                # okay, that's (likely) a footstep sound region.
                                bpy.ops.object.plasma_modifier_add(types="footstep")
                                kormanMod = blObj.plasma_modifiers.footstep
                                kormanMod.bounds = {
                                        pl.plSimDefs.kHullBounds: "hull",
                                        pl.plSimDefs.kProxyBounds: "trimesh",
                                        pl.plSimDefs.kExplicitBounds: "trimesh",
                                        pl.plSimDefs.kSphereBounds: "sphere",
                                        pl.plSimDefs.kCylinderBounds: "sphere",
                                        pl.plSimDefs.kBoxBounds: "box",
                                    }[sceneObj.sim.object.physical.object.boundsType]
                                kormanMod.surface = {
                                    0: "dirt",
                                    2: "puddle",
                                    3: "stone", # should be "tile". Used in Kadish Vault, Yeesha interior, but appears to play "stone" instead.
                                    4: "metal",
                                    5: "woodbridge",
                                    6: "rope",
                                    7: "grass",
                                    9: "woodfloor",
                                    10: "rug",
                                    11: "stone",
                                    13: "metal",
                                    14: "woodladder",
                                    15: "water",
                                    # 16 and 17: Gahreesen's maintainer suit footsteps. Using "water" instead - should make it clear it's not supported.
                                    16: "water",
                                    17: "water",
                                    18: "water", # Korman doesn't have swimming for some reason ?
                                    }[command.msg.surface]

                                # oh, and put some actual warning messages.
                                if command.msg.surface == 18:
                                    print("WARNING - Korman doesn't support \"swimming\" footstep sounds")
                                elif command.msg.surface == 16 or command.msg.surface == 17:
                                    print("WARNING - Korman doesn't support Gahreesen's maintainer suit footstep sounds")
                            else:
                                # TODO - some footstep sounds are not attached to their trigger region.
                                # So far this only happens in Jalak... We'll have to parse the whole objinvolume/logicmod/respmod chain someday...
                                print("WARNING - Footstep sounds bound to object %s, but triggered by another region. " % sceneObjKey.name \
                                    + "You will have to find the trigger region and add the modifier yourself.")

            elif modif.type == pl.plFactory.kSwimDetector:
                plMod = modif.object

                """
                # there are no swimdetectrgns in Korman. They are controlled by the swimregion itself.
                # So simply add the swim modifier to the main region and register ourself in it.
                msg = plMod.enterMsg if plMod.enterMsg else plMod.exitMsg
                self.setupReferenceLambdas.append(lambda:
                swimReg = self.parent.getBlObjectFromKey(msg.swimRegion)
                blObj.select = False
                swimReg.select = True
                bpy.context.scene.objects.active = swimReg
                bpy.ops.object.plasma_modifier_add(types="swimregion")
                kormanMod = swimReg.plasma_modifiers.swimregion
                swimReg.select = False
                blObj.select = True
                bpy.context.scene.objects.active = blObj
                kormanMod.region = blObj

                # the rest of the swim region's properties will be set when actually encountering the relevant modifier.
                #"""

                # ↑ uhhhh... The swim message's swimregion is the detector itself ?
                # Korman indicates otherwise, but that's how Ahnonay's detectors are setup. What the flying fuck... Bah, nevermind.

            elif modif.type == pl.plFactory.kCameraRegionDetector:
                bpy.ops.object.plasma_modifier_add(types="camera_rgn")
                plMod = modif.object
                kormanMod = blObj.plasma_modifiers.camera_rgn

                kormanMod.camera_type = "manual" # we already have a cam generated, so don't auto-generate one.
                for message in plMod.messages: # should only ever have one
                    def setCamObj(kormanMod, message):
                        kormanMod.camera_object = self.parent.getBlObjectFromKey(message.newCam)
                    self.setupReferenceLambdas.append(lambda: setCamObj(kormanMod, message))

                # dere ye go ! The rest of the message can be ignored.

            elif modif.type == pl.plFactory.kCameraModifier:
                # Thanksfully, this is already done in camImporter.
                pass

            elif modif.type == pl.plFactory.kAGModifier:
                # objectImporter takes care of this
                pass

            elif modif.type == pl.plFactory.kAGMasterMod:
                # objectImporter takes care of this
                pass

            elif modif.type == pl.plFactory.kRandomSoundMod:
                bpy.ops.object.plasma_modifier_add(types="random_sound")
                plMod = modif.object
                kormanMod = blObj.plasma_modifiers.random_sound

                # Collision, or regular random sound ?
                # To answer this, we need to find a plPhysicalSndGroup referencing this object. Which could be located in any PRP.
                isInSlide = False
                isInImpact = False
                groups = []
                for loc in self.parent.rmgr.getLocations():
                    for key in self.parent.rmgr.getKeys(loc, pl.plFactory.kPhysicalSndGroup):
                        if modif in key.object.slideSounds:
                            isInSlide = True
                            group = key.object.group
                            if group not in groups:
                                groups.append(group)
                        if modif in key.object.impactSounds:
                            isInImpact = True
                            group = key.object.group
                            if group not in groups:
                                groups.append(group)
                kormanMod.mode = "collision" if isInSlide or isInImpact else "random"
                if kormanMod.mode == "random":
                    kormanMod.play_mode = \
                        "coverall"   if hasFlags(plMod.mode, pl.plRandomSoundMod.kCoverall) else \
                        "norepeat"   if hasFlags(plMod.mode, pl.plRandomSoundMod.kNoRepeats) else \
                        "sequential" if hasFlags(plMod.mode, pl.plRandomSoundMod.kSequential) else \
                        "normal"
                    kormanMod.auto_start = plMod.state == pl.plRandomSoundMod.kStopped
                    kormanMod.stop_after_play = hasFlags(plMod.mode, pl.plRandomSoundMod.kOneCmd)
                    kormanMod.stop_after_set = hasFlags(plMod.mode, pl.plRandomSoundMod.kOneCmd)
                    kormanMod.min_delay = plMod.minDelay
                    kormanMod.max_delay = plMod.maxDelay
                else:
                    kormanMod.play_on = "impact" if isInImpact else "slide"
                    surfaces = set()
                    soundGroups = [
                        "kNone",
                        "kMetal", "kGrass", "kWood",
                        "kStone", "kWater", "kBone",
                        "kDirt", "kRug", "kCone",
                        "kUser1", "kUser2", "kUser3"
                    ]
                    for group in groups:
                        if group < len(soundGroups):
                            surfaces.add(soundGroups[group])
                    kormanMod.surfaces = surfaces

            elif modif.type == pl.plFactory.kGrassShaderMod:
                bpy.ops.object.plasma_modifier_add(types="grass_shader")
                plMod = modif.object
                kormanMod = blObj.plasma_modifiers.grass_shader
                for plWave, kWave in zip(plMod.waves, (kormanMod.wave1, kormanMod.wave2, kormanMod.wave3, kormanMod.wave4)):
                    kWave.distance = Vector((plWave.dist.X, plWave.dist.Y, plWave.dist.Z))
                    kWave.direction = Vector((plWave.dirX, plWave.dirY))
                    kWave.speed = plWave.speed

        blObj.select = False

    def setupReferences(self):
        # not really pretty, but so much damn simpler.
        for func in self.setupReferenceLambdas:
            func()

